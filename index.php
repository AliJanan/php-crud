<!DOCTYPE html>
<html lang="en">

<head>
    <title>Document</title>
    <!-- bootstrap 4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>

        <!-- CONNECT TWO PHP FILES (connect to conncetion.php)-->
        <?php include ("connection.php"); ?>

        <?php
    if (isset($_SESSION['message'])): ?>

        <!-- ALERT AND ALERT TYPE -->
        <div class="alert alert-<?=$_SESSION['msg_type']?>">

            <?php 
      echo $_SESSION['message']; 
      unset($_SESSION['message']); 
      ?>
        </div>

        <?php endif ?>

        <div class="container">
            <?php
      // CREATE CONNECTION
      $mysqli = new mysqli('localhost', 'root', 'root', 'php') or die(mysqli_error($mysqli)); 
      
      // GET DATA FROM DB
      $result = $mysqli->query("SELECT * FROM student") or die($mysqli->error); 
      
      ?>
            <div class="row justify-content-center">
                <table class="table">
                    <thead>
                        <tr>
                            <!-- <th scope="col">#</th> -->
                            <th scope="col">Name</th>
                            <th scope="col">Age</th>
                            <th scope="col" colspan="2">Action</th>
                        </tr>
                    </thead>

                    <?php
      while ($row = $result->fetch_assoc()): ?>

                    <!-- <tbody> -->
                    <tr>
                        <!-- <th scope="row">1</th> -->
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['age']; ?></td>
                        <td>
                            <a href="index.php?edit=<?php echo $row['id']; ?>" class="btn btn-info">Edit</a>
                            <a href="connection.php?delete=<?php echo $row['id']; ?>" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <!-- </tbody> -->

                    <?php endwhile; ?>
                </table>
            </div>

            <?php
        // PRINT ARRAY
        function pre_r( $array ) {
          echo '<pre>'; 
          print_r($array); 
          echo '</pre>'; 
        }
        ?>

            <div class="row justify-content-center">
                <form action="connection.php" method="POST">
                    <!-- HIDDEN INPUT -->
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" value="<?php echo $name; ?>" name="name"
                            placeholder="Enter your name">
                    </div>

                    <div class="form-group">
                        <label>Age</label>
                        <input type="text" class="form-control" value="<?php echo $age; ?>" name="age"
                            placeholder="Enter your age">
                    </div>


                    <div class="form-group">
                        <?php 
        if ($update == true): 
          ?>
                        <button type="submit" name="update" class="btn btn-primary">Update</button>
                        <?php else: ?>
                        <button type="submit" name="save" class="btn btn-primary">Save</button>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>

</body>

</html>